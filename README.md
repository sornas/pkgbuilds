# PKGBUILDs for [Arch User Repository](https://aur.archlinux.org)

Includes control scripts for managing AUR packages.

Forked from https://git.sr.ht/~dblsaiko/pkgbuilds.

## Commands

### setup.sh

Append ssh-config rules for accessing the AUR:
* `./setup.sh ssh`

Install [githooks](#hooks):
* `./setup.sh hooks`

### aurpublish

Push PACKAGE to the AUR. With "--speedup", merges the split history back in:
* `./aurpublish PACKAGE`

Pull package from the AUR (if you adopted an existing package, or have a co-maintainer):
* `./aurpublish -p PACKAGE`

View the git log of a package subtree:
* `./aurpublish log PACKAGE`

### feeds

Keep track of updates to your AUR packages via RSS feeds. Feed URLs are stored in `feeds.txt`.

Print all latest versions. Optionally only print outdated feeds.
* `./feeds [--only-outdated]`

Add a new feed URL. Checks for duplicates before adding.
* `./feeds add PACKAGE URL`

Remove a feed URL.
* `./feeds remove PACKAGE`

### Experimental

Download the history of a non-migrated AUR3 package, and commit it to a new subtree:
* `./import-from-aur3.sh PACKAGE`

## Hooks

Warn about whitespace errors, fail if checksums don't match, and auto-generate .SRCINFO for all changed PKGBUILDs:
* pre-commit

Prefill the commit message with a list of added/updated/deleted packages + versions (if any):
* prepare-commit-msg
